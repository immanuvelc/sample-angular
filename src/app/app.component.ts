import { Component } from '@angular/core';
import { faThumbsDown } from '@fortawesome/free-regular-svg-icons';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'EX3';
  ThumbsDown=faThumbsDown;
  Thumbsup=faThumbsUp;
  Thumbs:boolean=false;
  thumbs(){
    if(this.Thumbs==false){
      this.ThumbsDown=faThumbsUp;
      this.Thumbs=true;
    }
    else{
      this.ThumbsDown=faThumbsDown;
      this.Thumbs=false;
    }
  }
  }